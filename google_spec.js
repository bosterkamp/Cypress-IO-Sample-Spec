
//This is a test to validate the Google Title is "Google"
describe('Google Test', function () {
  
  //This describes the test.
  it('.should() - assert that <title> is correct', function () {

  	//Visit the Google site.
    cy.visit('https://www.google.com')

    //Validate the title includes Google
    cy.title().should('include', 'Google')


    })

	it('checks usaa.com is somewhere on the page', function() {

    //This just types in "usaa" to the search bar.
    cy.get('input[name=q]').type('usaa').type('{enter}')

        //Validate the title includes Google
    cy.get('a').should('contain', "usaa.com")

  })

})