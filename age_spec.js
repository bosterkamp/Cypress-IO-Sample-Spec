
//This is a test for AGE of Central Texas
describe('ageofcentraltx website Test', function () {
  
  //This describes the test.
  it('.should() - assert that <title> is correct', function () {

  	//Visit the AGE of Central Texas site.
    cy.visit('http://www.ageofcentraltx.org/')

    cy.title().should('include', 'AGE of Central Texas')

    })

    //This describes the test.
  it('.should() - assert the text "AGE" is on the page', function () {

    //Visit the AGE of Central Texas site.
    cy.visit('http://www.ageofcentraltx.org/')

    cy.contains('AGE')

    })

	it('checks the URL for a sign up is somewhere on the page', function() {

    //Visit the AGE of Central Texas site.
    cy.visit('http://www.ageofcentraltx.org/')

    //cy.contains("facebook.com")
    cy.get('a').should('contain', "Sign Up")

  })

    it('.should() - assert that Adult Day Health Centers is on the next page', function() {

    //Visit the AGE of Central Texas site.
    cy.visit('http://www.ageofcentraltx.org/')

    //This finds the id "dropmenu2" and then clicks on the element with "Adult" 
    //force: true is required because the element is not visible for some reason
   cy.get('#dropmenu2').contains('Adult').click({ force: true })

   cy.contains('Adult Day Health Centers')
  })

})